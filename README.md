> ⚠ Please note:  
> This README was retroactively created from the
> [original forum post](http://forum.sa-mp.com/showthread.php?t=292440)
> and was modified slightly to remove stuff like weird typos and expired
> links.

# RCONX
> v0.1

Hello out there!  
Today I present to you **<span style="color: #FF6600">RCON</span>X**, a
filterscript that can be used to access RCON without the need of really
be logged in as RCON Admin! Don't be scared, I'll explain 😀 

## The idea behind it
The idea behind **<span style="color: #FF6600">RCON</span>X** is, it
offers a few more abilities to you as default RCON system (f.ex a
logout function). Version 0.1 only includes the default<b>
<span style="color: red">*</span></b> functions of RCON, but there may
be more...

All you have to do is including this into your scripts (unter the includes) you're using RCON permissions in.
```c
#define IsPlayerAdmin(%0) GetPVarInt(%0, "RCONExLogin")
```

## Changelog
**[v0.1]**:
- First release

## Note
<span style="color: red"><b>*</b> These functions aren't included,
because without a fileplugin it's impossible to check wheather the files
exist or not:</span>
- loadfs
- reloadfs
- unloadfs
- exec

PS: Critics and comments are welcome 🙂  
I'd also like you to test it. I tested it before, but there may be some
errors, tho 🙂 
